// Random number generator
// ------------------------------------------------------
function numberGenerator(min, max) {                                     // Generates a random number from (and including) min to (and excluding) max.
    return Math.round(Math.random() * (max - min) + min);
}
const randomNumber = numberGenerator(0, 101);                            // Invokes the number generator with min = 0 and max = 101 and saves number to a constant "number".
// ------------------------------------------------------

// Restart button 
//--------------------------------------------------------
const reset = document.getElementById('reset'); // Defines the function to restart the game (by refreshing the page)
reset.addEventListener('click', function () {
    location.reload();
})
// ------------------------------------------------------

// Guess button with result generation
//--------------------------------------------------------
const guessButton = document.getElementById('guess-btn');         // The guess button is assigned to a js-variable. 
guessButton.addEventListener('click', function () {               // When the guess button is clicked, the game starts.
    var userGuess = document.getElementById('guess').value;       // The userGuess is defined by the value in input.

    if (userGuess < 0 || userGuess > 100 || isNaN(userGuess)) {   // Checks if input is a number and is between 0 and 100.
        alert('Your crazy guess is not valid here.');
    } else if (userGuess > randomNumber) {                        // Checks if input is too high.        
        alert('Nice try, but too high!');
    } else if (userGuess < randomNumber) {                        // Checks if input is too low.
        alert('Nice try but too low.');
    } else {                                                      // Input is not below 0 or 100, it is a number, and it is not too low or too high --> player "wins" 
        alert('Wow, you actually did it you maniac!');
        reset.style.visibility = 'visible';
    };

})
